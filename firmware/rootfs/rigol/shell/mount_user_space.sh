#!/bin/sh

YourInput=$1


SPACE_DATA=1
SPACE_USER=12

DATA_PATH=/rigol/data
USER_PATH=/user


########################################################
#. mount user partition
# p1= the number of the mtd
# p2= the number of the ubi,0 reserved
# p3= volume label

########################################################
format_mtd()
{
#only format user space
	#if [ $1 -eq $SPACE_USER ]; then
		if [ ! -c /dev/ubi$2_0 ]; then
			  $UBI_TOOL/ubidetach -d $2
			   #format MTD device number 0,            
		       #be quiet and force erase counter value 0
			  $UBI_TOOL/ubiformat /dev/mtd$1 -y -e 0

			  $UBI_TOOL/ubiattach -m $1 -d $2
			  $UBI_TOOL/ubimkvol  /dev/ubi$2 -N $3 -m
		else
			echo "Format failed" $3
		fi
	#fi
}

########################################################
#. mount user partition
# p1= the number of the mtd
# p2= the number of the ubi,0 reserved
# p3= the mount point. the user path
# p4= volume label

# ret: 0 OK, 1 first format and OK,2 NG
########################################################
UBI_TOOL=/ubifs-util
mount_mtd()
{
	ret=0
	
	#attach MTD device m to UBI              
	#and create UBI device number d
	$UBI_TOOL/ubiattach  -m $1 -d $2 >/dev/null 2>&1

	#Check if it is the first time to start
	if [ ! -c /dev/ubi$2_0 ]; then
		format_mtd $1 $2 $4
	fi

	mkdir -p $3
	mount -t ubifs -o sync /dev/ubi$2_0 $3

	if [ $? -ne 0 ]; then #can't mount user space
		format_mtd $1 $2 $4
		mount -t ubifs -o sync /dev/ubi$2_0 $3
		if [ $? -ne 0 ]; then
			rm -fr $3
			$UBI_TOOL/ubidetach -d $2
			ret=2
		fi
	fi
	return $ret
}


#Don't allow the kernel to output
echo 0 > /proc/sys/kernel/printk


if [ $YourInput -eq '0' ]; then
	#########################################################
	# mount data partition for Calibration and License data
	#########################################################
	mount_mtd $SPACE_DATA $SPACE_DATA $DATA_PATH "DATA"
	Result=$?
	if [ $Result -ne 0 ]; then
		if [ $Result -ne 1 ]; then
			echo 'mounting DATA partition failed'
			/rigol/tools/beeper 1
		else
			cp /rigol/default/*  $DATA_PATH
		fi
	fi
fi

if [ $YourInput -eq '1' ]; then
	sleep 20
	#########################################################
	#  mount user space to /user. here use the ubifs
	#  if mounted. then create 'data/doanload/ref' three dir
	#for application
	#########################################################
	mount_mtd $SPACE_USER $SPACE_USER $USER_PATH "USER"
	Result=$?
	if [ $Result -ne 0 ]; then

		if [ $Result -ne 1 ]; then
			echo 'mount user space failed'
			exit 1
		fi
	fi
	#for user file data
	mkdir -p $USER_PATH/data
	#for ref saving and loading
	mkdir -p $USER_PATH/ref
	#for update downloading
	mkdir -p $USER_PATH/download

	echo 'Mount user space to:'$USER_PATH
fi

#Allow the kernel to output
echo 7 > /proc/sys/kernel/printk

