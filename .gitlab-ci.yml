#
# SPDX-License-Identifier: AGPL-3.0+
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

stages:
  - lint
  - build
  - deploy
  - cleanup

# Common requisites
# =================
.parameters_common: &parameters_common
  tags:
    - docker

.changes_docker: &changes_docker
  changes:
    - Dockerfile
    - .dockerignore
    - dockerfiles/**/*
    - bin/gel_pack.sh
    - bin/gel_unpack.sh

# Linting
# All scripts/files should ideally be pushed through a linter to ensure
# code quality and consistency.

.linting:
  <<: *parameters_common
  stage: lint

shellscript_linting:
  extends: .linting
  image: registry.hub.docker.com/koalaman/shellcheck-alpine:stable
  only:
    changes:
      - dockerfiles/**/*.sh
  script:
    - find 'bin/' 'dockerfiles/' -iname '*.sh' -exec shellcheck -x -C -f tty -s sh {} \;

dockerfile_linting:
  extends: .linting
  image: registry.hub.docker.com/hadolint/hadolint:latest-debian
  only:
    changes:
      - Dockerfile
  script:
    - hadolint -f tty Dockerfile

# Prepare
# The prepare stage creates the container to build our package in
.build:
  <<: *parameters_common
  only:
    <<: *changes_docker
  image: registry.hub.docker.com/library/docker:stable

build:
  extends: .build
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - docker build --pull --rm -t "${CI_COMMIT_SHA}:${CI_PIPELINE_ID}" .
    - |
        docker run \
        --rm \
        "${CI_COMMIT_SHA}:${CI_PIPELINE_ID}" \
        /test/buildenv_check.sh

deploy:
  extends: .build
  stage: deploy
  only:
    <<: *changes_docker
    refs:
      - dev
  script:
    - docker login -u gitlab-ci-token -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
    - docker tag  "${CI_COMMIT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
    - docker tag  "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}" "${CI_REGISTRY_IMAGE}:lastest"
    - docker push "${CI_REGISTRY_IMAGE}:latest"

cleanup:
  extends: .build
  stage: cleanup
  when: always
  script:
    - |
      if docker inspect --type image "${CI_COMMIT_SHA}:${CI_PIPELINE_ID}" 1> /dev/null; then
        docker rmi "${CI_COMMIT_SHA}:${CI_PIPELINE_ID}"
      fi
