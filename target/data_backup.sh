#!/bin/sh -eu
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright 2019 (C) Olliver Schinagl <oliver@schinagl.nl>
#

set -eu

USB_STORAGE="${USB_STORAGE:-/media/sda1}"
DATA_PART="/rigol/data/"
BACKUP_FILENAME="${USB_STORAGE}/data_backup/data-$(date +%Y%m%d%H%M).tar"

if ! mountpoint "${DATA_PART}" || [ ! -d "${USB_STORAGE}/data_backup/" ]; then
    echo "ERROR: No mounted USB media."
    exit 1
fi

if [ -f "${BACKUP_FILENAME}" ]; then
    echo "ERROR: Backup file '${BACKUP_FILENAME}' already exists."
    exit 1
fi

tar -cf "${BACKUP_FILENAME}" "/rigol/data/"
[ -f "${BACKUP_FILENAME}" ] && \
    xz -9 "${BACKUP_FILENAME}" || \
    bzip2 -9 "${BACKUP_FILENAME}" || \
    gzip -9 "${BACKUP_FILENAME}" || \
    echo "WARN: Unable to compress tar, leaving as is."

exit 0
